package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TrazabilidadRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ITrazabilidadHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class trazabilidadRestControllerTest {


    @Mock
    private ITrazabilidadHandler trazabilidadHandler;

    @InjectMocks
    private TrazabilidadRestController trazabilidadRestController;

    @Test
    void generarTrazabilidad_ReturnsCreatedStatus() {
        TrazabilidadRequestDTO pedido = new TrazabilidadRequestDTO(1L,"","","","",1L,"");
        RegistroEntity registroEntity = new RegistroEntity();
        when(trazabilidadHandler.generarTrazabilidad(pedido)).thenReturn(registroEntity);

        ResponseEntity<RegistroEntity> response = trazabilidadRestController.generarTrazabilidad(pedido);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(registroEntity, response.getBody());
    }

    @Test
    void consultarTrazabilidadPedido_ReturnsListOfRegistroEntity() throws Exception {
        long idCliente = 123;
        String mail = "example@mail.com";
        String password = "password";
        List<RegistroEntity> registros = new ArrayList<>();
        when(trazabilidadHandler.consultarTrazabilidadPedido(idCliente, mail, password)).thenReturn(registros);

        List<RegistroEntity> response = trazabilidadRestController.consultarTrazabilidadPedido(idCliente, mail, password);

        assertEquals(registros, response);
    }

    @Test
    public void testGetFechasRegistros() {
        Long idPedido = 1L;
        RegistroEntity registroPendiente = new RegistroEntity();
        registroPendiente.setFecha(LocalDateTime.now());
        RegistroEntity registroListo = new RegistroEntity();
        registroListo.setFecha(LocalDateTime.now().plusHours(1));
        when(trazabilidadHandler.findStartPendingOrder(idPedido)).thenReturn(registroPendiente);
        when(trazabilidadHandler.findEndReadyOrder(idPedido)).thenReturn(registroListo);

        ResponseEntity<Map<String, Object>> response = trazabilidadRestController.getFechasRegistros(idPedido);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Map<String, Object> fechasRegistros = response.getBody();
        assertNotNull(fechasRegistros);
        assertEquals(registroPendiente.getFecha(), fechasRegistros.get("registroPendiente"));
        assertEquals(registroListo.getFecha(), fechasRegistros.get("registroListo"));
    }

    @Test
    public void testGetFastestOrders() {
        ResponseEntity<List<Map<String, Object>>> response = trazabilidadRestController.getFastestOrders();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

}