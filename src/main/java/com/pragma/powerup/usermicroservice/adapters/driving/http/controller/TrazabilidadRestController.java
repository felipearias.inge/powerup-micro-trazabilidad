package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TrazabilidadRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ITrazabilidadHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/trazabilidad")
@RequiredArgsConstructor
public class TrazabilidadRestController {

    @Autowired
    public ITrazabilidadHandler trazabilidadHandler;




    @PostMapping
    public ResponseEntity<RegistroEntity> generarTrazabilidad(@RequestBody TrazabilidadRequestDTO pedido) {
        RegistroEntity registro = trazabilidadHandler.generarTrazabilidad(pedido);
        return new ResponseEntity<>(registro, HttpStatus.CREATED);
    }

    @GetMapping("/consultar")
    public List<RegistroEntity> consultarTrazabilidadPedido(
            @RequestParam Long idPedido,
            @RequestParam String mail,
            @RequestParam String password
    ) throws Exception {
        return trazabilidadHandler.consultarTrazabilidadPedido(idPedido, mail, password);
    }

    @GetMapping("/fechas/{idPedido}")
    public ResponseEntity<Map<String, Object>> getFechasRegistros(@PathVariable("idPedido") long idPedido) {
        RegistroEntity registroPendiente = trazabilidadHandler.findStartPendingOrder(idPedido);
        RegistroEntity registroListo = trazabilidadHandler.findEndReadyOrder(idPedido);

        Map<String, Object> fechasRegistros = new HashMap<>();

        LocalDateTime fechaPendiente = registroPendiente.getFecha();
        LocalDateTime fechaListo = registroListo.getFecha();
        Duration duracion = Duration.between(fechaPendiente,fechaListo);

        long horas = duracion.toHours();
        long minutos = duracion.toMinutesPart();
        long segundos = duracion.toSecondsPart();

        fechasRegistros.put("registroPendiente", fechaPendiente);
        fechasRegistros.put("registroListo", fechaListo);
        fechasRegistros.put("duracion", String.format("%02d:%02d:%02d", horas, minutos, segundos));
        return ResponseEntity.ok(fechasRegistros);

    }

    @GetMapping("/ranking")
    public ResponseEntity<List<Map<String, Object>>> getFastestOrders() {
        List<RegistroEntity> registrosListo = trazabilidadHandler.findAllEndReadyOrders();
        List<Map<String, Object>> ranking = new ArrayList<>();

        for (RegistroEntity registroListo : registrosListo) {
            long idPedido = registroListo.getIdPedido();
            RegistroEntity registroPendiente = trazabilidadHandler.findStartPendingOrder(idPedido);
            String idEmpleado = trazabilidadHandler.findIdEmpleadoByIdPedido(idPedido);

            if (registroPendiente != null) {
                LocalDateTime fechaPendiente = registroPendiente.getFecha();
                LocalDateTime fechaListo = registroListo.getFecha();
                Duration duracion = Duration.between(fechaPendiente, fechaListo);

                long horas = duracion.toHours();
                long minutos = duracion.toMinutesPart();
                long segundos = duracion.toSecondsPart();

                Map<String, Object> registroInfo = new HashMap<>();
                registroInfo.put("idPedido", idPedido);
                registroInfo.put("duracion", String.format("%02d:%02d:%02d", horas, minutos, segundos));
                registroInfo.put("idEmpleado",idEmpleado);

                ranking.add(registroInfo);
            }
        }
        ranking.sort(Comparator.comparing(registro -> registro.get("duracion").toString()));

        return ResponseEntity.ok(ranking);
    }

}
