package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TrazabilidadRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ITrazabilidadHandler;
import com.pragma.powerup.usermicroservice.domain.repositories.ITrazabilidadRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TrazabilidadHandlerImpl implements ITrazabilidadHandler {

    @Autowired
    private ITrazabilidadRepository trazabilidadRepository;


    public String obtenerIdEmpleado(String mail) {

        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/user/" + mail + "/id"))
                .GET()
                .build();

        HttpResponse<String> ownerResponse;
        try {
            ownerResponse = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        String idEmpleado = ownerResponse.body();

        return idEmpleado;
    }


    @Override
    public RegistroEntity generarTrazabilidad(TrazabilidadRequestDTO pedido) { //Se pone el en DTO lo sisguiente

        LocalDateTime fechaActual = LocalDateTime.now();
        RegistroEntity registro = new RegistroEntity();
        registro.setIdPedido(pedido.getIdPedido());
        registro.setIdCliente(pedido.getIdCliente());
        registro.setCorreoCliente(pedido.getCorreoCliente());
        registro.setFecha(fechaActual);
        registro.setEstadoAnterior(pedido.getEstadoAnterior());
        registro.setEstadoNuevo(pedido.getEstadoNuevo());
        registro.setIdEmpleado(pedido.getIdEmpleado());
        registro.setCorreoEmpleado(pedido.getCorreoEmpleado());

        return trazabilidadRepository.save(registro);
    }

    public String getRole(String mail, String password) {
        HttpClient client = HttpClient.newHttpClient();

        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        String responseBody = response.body();

        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        String role = jwtResponseDto.getRoles().get(0);

        return role;
    }

    @Override
    public List<RegistroEntity> consultarTrazabilidadPedido(Long idPedido, String mail, String password) throws Exception {
        String role = getRole(mail, password);


         //long idCliente = Long.parseLong(obtenerIdEmpleado(mail));


        System.out.println("Descripcion: " + role);
        if (!role.equalsIgnoreCase("ROLE_CUSTOMER")) {
            throw new Exception("THE USER IS NOT A CUSTOMER PLEASE TRY AGAIN");
        }

        return trazabilidadRepository.findByIdPedido(idPedido);
    }


    public RegistroEntity findStartPendingOrder(long idPendiente) {
        RegistroEntity registro = trazabilidadRepository.findFechaByIdPedidoAndEstadoNuevoPendiente(idPendiente);
        return registro;
    }

    public RegistroEntity findEndReadyOrder(long idPendiente) {
        return trazabilidadRepository.findFechaByIdPedidoAndEstadoNuevoListo(idPendiente);

    }

    public List<RegistroEntity> findAllEndReadyOrders() {
        return trazabilidadRepository.findByEstadoNuevo("Listo");
    }

    public String findIdEmpleadoByIdPedido(long idPedido) {
        String resultadoConsulta = trazabilidadRepository.findIdEmpleadoByIdPedido(idPedido);
        JSONObject jsonObject = new JSONObject(resultadoConsulta);

        int idEmpleado = jsonObject.getInt("idEmpleado");


        return String.valueOf(idEmpleado);
    }


}
