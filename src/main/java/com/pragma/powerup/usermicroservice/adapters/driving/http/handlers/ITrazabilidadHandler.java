package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TrazabilidadRequestDTO;

import java.util.List;

public interface ITrazabilidadHandler {

    RegistroEntity generarTrazabilidad(TrazabilidadRequestDTO pedido);
    List<RegistroEntity> consultarTrazabilidadPedido(Long idPedido, String mail, String password) throws Exception;

     RegistroEntity findStartPendingOrder(long idPendiente);

     RegistroEntity findEndReadyOrder(long idPendiente);

    List<RegistroEntity> findAllEndReadyOrders();

    String findIdEmpleadoByIdPedido(long idPedido);


}
